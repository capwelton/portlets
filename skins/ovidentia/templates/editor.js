
jQuery(document).ready(function() {

	jQuery('.widget-button').click(function(evt) {
		
		
		
		var button = jQuery(this);
		var form = button.closest('.widget-form');
		
		var reference = form.find('input[name=portlet_reference]').attr('value');
		var params = new Object();
		
		form.find('input, select').each(function(pos, item) {
			item = jQuery(item);
			
			if (!item.attr('name')) {
				return;
			}

			if (match = item.attr('name').match(/preferences\[([0-9a-zA-Z_]+)\]/)) {
				if (item.get(0).tagName == 'SELECT') {
					params[match[1]] = item.find('option:selected').attr('value');
				} else {
				    if (item.attr('type') === 'checkbox') {
				        params[match[1]] = (item.is(":checked") ? '1' : '0');
				    } else {
				        params[match[1]] = item.attr('value');
				    }
				}
				
			}
		});
		
		var uniqueID = new Date();
		params['__id'] = 'E' + uniqueID.getTime();
		
		
		var r = reference +'?'+ jQuery.param(params);
		
		
		opener.bab_dialog._return({
			'callback' 	: 'editorInsertText',
			'param'		: '<img src="'+bab_getInstallPath()+'skins/ovidentia/images/addons/portlets/portlet_placeholder.png" longdesc="'+r+'" />'
		});
		
		window.close();
	});

});

