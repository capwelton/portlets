// loaded on page refresh


window.babAddonportlets = {};


window.babAddonportlets.lastOpenedContainer = null;

/**
 * @param	string	prefix
 * return Array
 */
window.babAddonportlets.getIdsByPrefix = function(prefix)
{
	var arr = new Array();
	var search = new RegExp(prefix+'([A-Za-z0-9_-]+)');

	jQuery('[class*="'+prefix+'"]').each(function(k, item) {

		var m = jQuery(item).attr('class').match(search);
		if (null !== m && null != m[1])
		{
			var id = m[1];
			if (-1 == jQuery.inArray(id, arr))
			{
				arr.push(id);
			}
		}
	});

	return arr;
};





window.babAddonportlets.callUrl = function()
{
	return '?tg=addon/portlets/call';
	//return '?addon=portlets.call';
};





/**
 * Add a contextual button on the container
 */
jQuery.fn.addContainerButton = function(callback, classname, title)
{
	this.each(function(i, element) {

		element = jQuery(element);
		element.find('.portlets-button').remove();

		element.hover(
			function() {

				if (window.draggingPortlet === true) {
					return;
				}
				var item = jQuery(this);
				if (item.closest('.portlets-edit-mode').length == 0) {
					return;
				}

				if (item.find('.portlets-button ' + '.' + classname).length != 0) {
					return;
				}

				item.addClass('portlets-item-hover');
				var button = jQuery('<span></span>');
				item.append(button);
				button.addClass('portlets-button');
				button.addClass('portlets-hoverbutton');
				button.addClass(classname);
				button.attr('title', title);
				button.on('click', callback);

				if (button.offset().top < 0) {
					bottom = button.parent().height();
					button.css('top', bottom+'px');
				}

				if (button.prev().is('.portlets-button'))
				{
					var left = (button.prev().position().left + 22);
					button.css('left', left+'px');
				}

			},
			function() {
				var item = jQuery(this);
				item.removeClass('portlets-item-hover');
				item.find('.portlets-button').remove();
			}
		);

	});
};



window.babAddonportlets.initContainer = function(containerId)
{

	var container = jQuery('.bab-portlets-container-' + containerId);

	if (container.hasClass('portlets-configurable')) {

		container.addContainerButton(function() {

			window.babAddonportlets.lastOpenedContainer = jQuery(this).parent('.bab-portlet-container');

			var dock = jQuery('.widget-dockable');

			if (dock.length == 0) {
				var button = jQuery(this);
				button.addClass('portlets-loading');
				window.babAddonportlets.initDockable();
				button.removeClass('portlets-loading');
			} else if ('closed' == dock.getDockStatus()) {
				dock.toggleDock();
			}

		}, 'portlets-edit', 'Ajouter des portlets \u00E0 cette zone');
	}

	if (container.hasClass('portlets-lockable')) {

		var buttonClassname = '';
		var buttonTitle = '';
		if (container.hasClass('portlets-locked')) {
			buttonClassname = 'portlets-unlock';
			buttonTitle = 'D\u00E9verrouiller cette zone';
		} else {
			buttonClassname = 'portlets-lock';
			buttonTitle = 'Verrouiller cette zone';
		}
		container.addContainerButton(function() {

			// this is the clicked button span
			var container = jQuery(this).closest('.bab-portlet-container');
			var containerId = window.babAddonportlets.getContainerId(container);
		    jQuery.ajax({
				url: window.babAddonportlets.callUrl() + '&idx=toggleLockContainer&container=' + containerId,
				success: function(data) {
					if ('' !== data)
					{
						jQuery(container).replaceWith(data);
						container = jQuery('.bab-portlets-container-' + containerId);
						window.bab.init(container);
						window.babAddonportlets.initContainer(containerId);
					}
				}
			});

		}, buttonClassname, buttonTitle);
	}

	if (container.hasClass('portlets-sortable')) {

		var connectWith = '';

		if (container.hasClass('portlets-locked')) {
			connectWith = '.portlets-sortable.portlets-locked';
		} else {
			connectWith = '.portlets-sortable:not(.portlets-locked)';
		}

		container.sortable({
			handle: '.portlet-toolbar .widget-title',
			items: '.portlet-container-item',
			helper: function() {return jQuery('<div class="portlet-frame-helper"></div>');},
			placeholder: 'ui-sortable-placeholder',
			tolerance: 'pointer',
			connectWith: connectWith,

			start: function(event, ui) {
				var containers = jQuery('.bab-portlet-container');
				containers.removeClass('portlets-item-hover');
				containers.find('.portlets-button').remove();
				console.debug(ui.item.get(0).className);
				ui.helper.addClass(ui.item.get(0).className);
				ui.placeholder.addClass(ui.item.get(0).className);
				window.draggingPortlet = true;
			},
			stop: function(event, ui) {
				window.draggingPortlet = false;
			},
			update: function(event, ui) {

				var container = jQuery(this);
				var items = new Array();
				container.find('.portlet-frame').each(function(i, frame) {
					items.push(jQuery(frame).attr('id').split('_')[1]);
				});

				var id = window.babAddonportlets.getContainerId(container);

				var values = {
					container: id,
					portlets: items
				};

				jQuery.ajax({
					url: window.babAddonportlets.callUrl() + '&idx=movePortlets',
					data: values,
					success: function(data) {
						window.babAddonportlets.checkEmptyContainers();
					}
				});
			}
		});
	}
};



window.babAddonportlets.parseContainers = function()
{
	var portlets = window.babAddonportlets.getIdsByPrefix('bab-portlets-container-');

	if (portlets.length) {

		for (var i = 0; i < portlets.length; i++)
		{
			window.babAddonportlets.initContainer(portlets[i]);
		}
	}
};


/**
 * if dockable does not exists, load html and execute javascript
 */
window.babAddonportlets.initDockable = function()
{

	jQuery.ajax({
		url : window.babAddonportlets.callUrl() + '&idx=loaddockable',
		async : false,
		dataType: 'json',
		success : function(data) {

			if (data.dockable)
			{
				jQuery("body").append(data.dockable);
			}
		}
	});
}


function portlet_initKeywordTimeout()
{
	if(jQuery('.portlet-search-form input[type="text"]').length > 0){
		jQuery('.portlet-search-form input[type="text"]').keyup(function(){
			window.babAddonportlets.clickSearchDockable();
		});
	}else{
		setTimeout(function(){
			portlet_initKeywordTimeout();
		}, 200);
	}
}

function portlet_initCategoryTimeout()
{
	if(jQuery('.portlet-search-form select').length > 0){
		window.babAddonportlets.clickSearchDockable();

		jQuery('.portlet-search-form select').change(function(){
			window.babAddonportlets.clickSearchDockable();
		});
	}else{
		setTimeout(function(){
			portlet_initCategoryTimeout();
		}, 200);
	}
}

function potlet_initScrollBackground()
{
	if(jQuery('.widget-dockable-fullsize-background.open').length > 0){
		jQuery('body').addClass('stop-scrolling');
		potlet_endtScrollBackground();

	}else{
		setTimeout(function(){
			potlet_initScrollBackground();
		}, 200);
	}
}

function potlet_endtScrollBackground()
{
	if(jQuery('.widget-dockable-fullsize-background.closed').length > 0){
		jQuery('body').removeClass('stop-scrolling');
		potlet_initScrollBackground();
	}else{
		setTimeout(function(){
			potlet_endtScrollBackground();
		}, 200);
	}
}


function portlets_updateEditMode()
{
	if (jQuery('body').hasClass('portlets-edit-mode')) {
		jQuery('.portlets-sortable').sortable('enable');
	} else {
		jQuery('.portlets-sortable').sortable('disable');
	}
}



window.babAddonportlets.getContainerId = function(container)
{
	var html_id = null;
	if (container.length === 0) {
		return null;
	}
	var search = new RegExp('bab-portlets-container-([A-Za-z0-9_-]+)');
	var classes = container.attr('class').split(/\s+/);
	for (var i = 0; i < classes.length; i++) {
		var m = classes[i].match(search);
		if (null !== m && null != m[1]) {
			html_id = m[1];
			break;
		}
	}

	return html_id;
};



window.babAddonportlets.clickDefinition = function(definition)
{
	var target = window.babAddonportlets.lastOpenedContainer;
	if (null == target) {
		target = jQuery('.bab-portlet-container:first');
	}

	if (null == target) {
		alert('No container found');
	}

	// ajax loading frame

	var layoutItem = jQuery('<span class="widget-layout-item portlet-container-item"></span>');
	target.prepend(layoutItem);
	var frame = jQuery('<div class="portlet-loading"></div>');
	layoutItem.prepend(frame);

	var meta = window.babAddonWidgets.getMetadata(definition.attr('id'));

	var container = window.babAddonportlets.getContainerId(target);

	if (null === container)
	{
//		alert('container not found');
//		return;
	}

	
	var url = window.babAddonportlets.callUrl() + '&idx=create&backend='+meta.backend+'&portlet_definition_id='+meta.portlet_definition_id+'&container='+container;

	frame.load(url, function() {
		frame.find('.portlet-frame').prependTo(layoutItem);
		window.bab.init(layoutItem);
		frame.remove();
		window.babAddonportlets.checkEmptyContainers();
	});
	jQuery('.widget-dockable-fullsize-background.open').trigger('click');
};



window.babAddonportlets.clickPreferences = function(preferences)
{
	var portletFrame = preferences.closest('.portlet-frame');
	var preferenceFrame = portletFrame.find('.portlet-edit-settings-frame');

	if (preferenceFrame.length == 0)
	{
		// The preference frame does not exist, we fetch it from the server.

		preferenceFrame = jQuery('<div class="portlet-edit-settings-frame preferences-loading"></div>');
		portletFrame.find('.portlet-toolbar').after(preferenceFrame);

		var portletFrameId = portletFrame.attr('id');

		var portletId = portletFrameId.split('_')[1];
		var url = window.babAddonportlets.callUrl() + '&idx=preferences&portlet=' + portletId;

		var target = portletFrame.closest('.bab-portlet-container');

		var containerId = window.babAddonportlets.getContainerId(target);

		jQuery(preferenceFrame).dialog({
			title: 'Configuration',
            width: 'auto',
            height: 'auto',
			modal: true
		});

		preferenceFrame.load(url, function() {

			window.bab.init(preferenceFrame);

			preferenceFrame.removeClass('preferences-loading');

			preferenceFrame.find('form').submit(function() {

				var portlet = portletFrame.attr('id').split('_')[1];

				var values = jQuery(this).serializeArray();
				jQuery.post({
					url: window.babAddonportlets.callUrl() + '&idx=savePreferences&portlet=' + portlet + '&container=' + containerId,
					data: values,
					success: function(data) {
						// Update portlet frame content
						portletFrame.replaceWith(data);
						window.bab.init(document.getElementById(portletFrameId));
						jQuery(preferenceFrame).dialog('close');
					}
				});
				return false;
			});
		});
	} else {
		preferenceFrame.toggle();
	}

};



window.babAddonportlets.clickClose = function(close) {

	if (confirm(close.attr('title')))
	{
		var frame = close.closest('.portlet-frame');
		var portlet = frame.attr('id').split('_')[1];
		jQuery.ajax({
			url : window.babAddonportlets.callUrl() + '&idx=close&portlet='+portlet,
			success: function( data ) {
				if ('' == data)
				{
					frame.slideUp(function() {
						frame.remove();
						window.babAddonportlets.checkEmptyContainers();
					});


				} else {
					alert('clickClose: ' + data);
				}
			}
		});

	}
};




window.babAddonportlets.clickSearchDockable = function() {
	var containerId = window.babAddonportlets.getContainerId(window.babAddonportlets.lastOpenedContainer);
	var resultFrame = jQuery('.portlet-search-results');
	var form = jQuery('.portlet-search-form');
	var keyword = form.find('input[name=keyword]').attr('value');
	var category = form.find('select[name="category[]"]').val();

	if (category) {
		category = category.join(',');
	} else {
		category = window.bab["portletcontainercategories-" + containerId];
	}

	resultFrame.empty();
	resultFrame.addClass('portlet-search-loading');

	resultFrame.load(window.babAddonportlets.callUrl() + '&container='+encodeURIComponent(window.babAddonportlets.lastOpenedContainer.attr('id'))+'&idx=searchPortlet&keyword='+encodeURIComponent(keyword)+'&category='+encodeURIComponent(category), function() {
		resultFrame.removeClass('portlet-search-loading');
	});
};



window.babAddonportlets.checkEmptyContainers = function() {
	jQuery('.bab-portlet-container').each(function () {
		var containedPortlets = jQuery(this).find('.portlet-frame').length;
		if (containedPortlets == 0) {
			jQuery(this).addClass('portlets-empty');
		} else {
			jQuery(this).removeClass('portlets-empty');
		}
	});

};




jQuery(document).ready(function() {
	window.babAddonportlets.parseContainers();
	window.babAddonportlets.checkEmptyContainers();


	var containers = jQuery('.bab-portlet-container.portlets-lockable, .bab-portlet-container.portlets-sortable, .bab-portlet-container.portlets-sortable');

	if (containers.length > 0) {
		var toggleButton = jQuery('.portlets-toggle-edit-mode');
		if (toggleButton.length == 0) {
			toggleButton = jQuery('<div class="portlets-toggle-edit-mode">Edit</div>');
			jQuery('body').append(toggleButton);
		}
		toggleButton.on('click', function () {
			jQuery('body').toggleClass('portlets-edit-mode');
			portlets_updateEditMode();
		});

		portlets_updateEditMode();
	}

	//setTimeout(function(){
		portlet_initKeywordTimeout();
		portlet_initCategoryTimeout();
		potlet_initScrollBackground();
	//}, 200);
});



jQuery(document).on('click', function(evt) {
	var definition = jQuery(evt.target).closest('.portlet-definition');
	if (0 != definition.length) {
		window.babAddonportlets.clickDefinition(definition);
	}

	var preferences = jQuery(evt.target).closest('.portlet-edit-settings');

	if (0 != preferences.length) {
		window.babAddonportlets.clickPreferences(preferences);
	}

	var close = jQuery(evt.target).closest('.portlet-close');

	if (0 != close.length) {
		window.babAddonportlets.clickClose(close);
	}

	window.bab["portletcontainercategories-" + containerId] = null;

	if (window.babAddonportlets.lastOpenedContainer) {
		var containerId = window.babAddonportlets.getContainerId(window.babAddonportlets.lastOpenedContainer);
		var containerCategory = window.bab["portletcontainercategories-" + containerId];

		var searchDockable = jQuery(evt.target).closest('.portlet-search-dockable');

		if (0 != searchDockable.length || window.babAddonportlets.currentDockableCategory != containerCategory) {
			evt.stopPropagation();
			window.babAddonportlets.currentDockableCategory = containerCategory;
			window.babAddonportlets.clickSearchDockable();
			return false;
		}
	}
});
