<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


// Initialize ORM backend.
if (!class_exists('ORM_RecordSet')) {
    bab_functionality::get('LibOrm')->initMySql();
}
$ormBackend = ORM_RecordSet::getBackend();
if (!isset($ormBackend)) {
    $ormBackend = new ORM_MySqlBackend($GLOBALS['babDB']);
    ORM_RecordSet::setBackend($ormBackend);
}


/**
 * @property ORM_StringField $backend
 * @property ORM_StringField $portletDefinition
 * @property ORM_BoolField   $active
 *
 * @method portlets_PortletDefinitionConfiguration      get()
 * @method portlets_PortletDefinitionConfiguration[]    select()
 */
class portlets_PortletDefinitionConfigurationSet extends ORM_RecordSet
{
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
				ORM_StringField('backend'),
				ORM_StringField('portletDefinition'),
				ORM_BoolField('active')
		);

	}
}


/**
 * @property string $backend
 * @property string $portletDefinition
 * @property bool   $active
 */
class portlets_PortletDefinitionConfiguration extends ORM_Record
{

}
