<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


function portlets_getList()
{
	$W = bab_Widgets();

	$page = $W->BabPage(null, $W->VBoxLayout());
	$page->setEmbedded(false);

	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	require_once dirname(__FILE__) . '/portletdefinitionconfiguration.class.php';

	$func = new bab_Functionalities();

	$backendNames = $func->getChildren('PortletBackend');

	bab_functionality::includefile('PortletBackend');
	bab_functionality::includefile('Icons');

	$portletDefinitionConfigurationSet = new portlets_PortletDefinitionConfigurationSet();


	foreach ($backendNames as $backendName) {
		$backendPath = trim('PortletBackend/' . $backendName, '/');


		/* @var $backend Func_PortletBackend */
		$backend = bab_Functionality::get($backendPath);

		if (!$backend) {
		    continue;
		}

		$backendSection = $W->Section(
				$backend->getDescription(),
				$W->VBoxLayout()
		)->setFoldable(true)->addClass(Func_Icons::ICON_LEFT_16);

		$backendAvailablePortletsDef = $backend->select();

		$nbPortletDefinitions = 0;
		foreach ($backendAvailablePortletsDef as $definition) {

			$portletDefinitionConfiguration = $portletDefinitionConfigurationSet->get(
					$portletDefinitionConfigurationSet->backend->is($backendName)
					->_AND_($portletDefinitionConfigurationSet->portletDefinition->is($definition->getId()))
			);

			if ($portletDefinitionConfiguration && !$portletDefinitionConfiguration->active) {
				continue;
			}

			$nbPortletDefinitions++;

			require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

			$url = bab_url::get_request('tg');
			$url->idx = 'configure';
			$url->backend = $backendName;
			$url->definition_id = $definition->getId();

			$iconUrl = $definition->getIcon();
			$icon = $W->Image($iconUrl)->setCanvasOptions(Widget_Item::Options()->width(48, 'px'));

			$backendSection->addItem(
				$W->Frame(null, $W->HboxLayout()->setSpacing(0, 'px', 5, 'px'))->addClass('portlet-definition')
					->addItem($icon)
					->addItem($W->Link($definition->getName(), $url->toString())->setTitle($definition->getDescription()))
					->setMetadata('backend', $backendName)
					->setMetadata('portlet_definition_id', $definition->getId())
			);

		}

		if ($nbPortletDefinitions > 0) {
			$page->addItem($backendSection);
		}
	}


	$page->displayHtml();
}




function portlets_configure($backendName, $definition_id)
{
    require_once dirname(__FILE__).'/preferencesform.php';
    require_once $GLOBALS['babInstallPath'].'utilit/reference.class.php';
    $W = bab_Widgets();

    $backendPath = trim('PortletBackend/' . $backendName, '/');
    /* @var $backend Func_PortletBackend */
    $backend = bab_Functionality::get($backendPath);

    $addon = bab_getAddonInfosInstance('portlets');

    $page = $W->BabPage(null, $W->VBoxLayout());
    $page->setEmbedded(false);
    $page->addJavascriptFile($addon->getTemplatePath().'editor.js?' . $addon->getDbVersion());

    $portletDefinition = $backend->getPortletDefinition($definition_id);


    $form = portlets_getPreferencesForm($portletDefinition);

    $form->addClass('portlet-preferences-form');
    $page->addItem($form);

    $ref = bab_Reference::makeReference('ovidentia', '', 'portlets', 'portlet', "$backendName/$definition_id");

    $form->setHiddenValue('portlet_reference', $ref->__toString());

    $form->additem($W->Button()->addItem($W->Label(portlets_translate('Add portlet'))));
    $page->displayHtml();
}



// main


$idx = bab_rp('idx');
if (empty($idx))
{
	$idx = 'select';
}


switch($idx)
{
	case 'select':
		portlets_getList();
		break;

	case 'configure':
		portlets_configure(bab_rp('backend'), bab_rp('definition_id'));
		break;

}