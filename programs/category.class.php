<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


// Initialize mysql ORM backend.
bab_functionality::get('LibOrm')->initMySql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($GLOBALS['babDB']));



/**
 *
 * @property ORM_PkField          $id
 * @property ORM_StringField      $name
 * @property ORM_StringField      $from
 * @property ORM_Boolfield        $disabled
 */
class portlets_CategorySet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name'),
			ORM_StringField('from'),
			ORM_Boolfield('disabled')
		);

	}
}


/**
 *
 * @property ORM_PkField          $id
 * @property ORM_StringField      $name
 * @property ORM_StringField      $from
 * @property ORM_BoolfieldField   $disabled
 */
class portlets_Category extends ORM_MySqlRecord
{
}
