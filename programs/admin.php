<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/portletdefinitionconfiguration.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';



bab_Widgets()->includePhpClass('widget_TableView');


class portlet_categoryTableView extends widget_TableModelView
{
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        if ($fieldPath === '_action_') {
            return 	$W->Link(
                $W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
                '?tg=addon/portlets/admin&idx=delete_category&id='.$record->id
            )->addClass(Func_Icons::ICON_LEFT_24)->setTitle(portlets_translate('Remove'))
            ->setConfirmationMessage(portlets_translate('This will delete this category?'));
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}


function portlet_displayAvailableBackends()
{
    $I = bab_Functionality::get('Icons');
    $I->includeCss();
    $W = bab_Widgets();

    $page = $W->BabPage();
    $page->setLayout($W->VBoxLayout());

    $page->addItem(
        $W->Title(portlets_translate('Portlets management'), 1)
            ->addClass('title')
    );

    $addon = bab_getAddonInfosInstance('portlets');

    $page->addItemMenu('backends', portlets_translate('Portlets'), $addon->getUrl().'admin&idx=backends');
    $page->addItemMenu('portlet_managers', portlets_translate('Portlet managers'), $addon->getUrl().'admin&idx=portlet_managers');
    $page->addItemMenu('categories', portlets_translate('Categories management'), $addon->getUrl().'admin&idx=category');
    $page->setCurrentItemMenu('backends');

    $form = $W->Form();
    $form->setName('definitions');
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('idx', 'set_active_portlet_definitions');

    $form->setLayout(
        $W->VBoxLayout()
            ->setVerticalSpacing(2, 'em')
    );

    $form->addClass('portlets-admin-form');

    $cssClasses = array(
        0 => 'BabForumBackground1',
        1 => 'BabSiteAdminFontBackground'
    );

    $backurl = bab_url::request('tg');


    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    $func = new bab_Functionalities();

    $backendNames = $func->getChildren('PortletBackend');

    bab_functionality::includefile('PortletBackend');
    bab_functionality::includefile('Icons');

    foreach ($backendNames as $backendName) {
        $backendPath = trim('PortletBackend/' . $backendName, '/');


        /* @var $backend Func_PortletBackend */
        $backend = bab_Functionality::get($backendPath);

        if (!$backend) {
            bab_debug('Portlet backend unavailable: ' . $backendPath);
            continue;
        }

        $backendSection = $W->Section(
            $backend->getDescription(),
            $W->VBoxLayout()
        );


        $backendSection->setName(trim($backendName, '/'));
        $backendSection->setFoldable(true);

        $backendConfigurationActions = $backend->getConfigurationActions();

        if (count($backendConfigurationActions) > 0) {
            $backendSectionMenu = $backendSection->addPopupMenu();

            foreach ($backendConfigurationActions as $backendConfigurationAction) {
                /* @var $backendConfigurationAction Widget_Action */
                $backendConfigurationAction->setParameter('backurl', $backurl);
                $backendSectionMenu->addAction($backendConfigurationAction);
            }
        }

        $oddEven = 0;

        $backendAvailablePortletsDef = $backend->select();
        foreach ($backendAvailablePortletsDef as $definition) {
            $iconUrl = $definition->getIcon();
            $icon = $W->Image($iconUrl)->setCanvasOptions(Widget_Item::Options()->width(48, 'px'));

            $description = $definition->getDescription();
            if (empty($description)) {
                $description = portlets_translate('No description available.');
            }

            $definitionSection = $W->Frame()->setLayout(
                $W->HBoxItems(
                    $icon,
                    $W->VBoxItems(
                        $title = $W->Title($definition->getName(), 6),
                        $W->RichText($description)
                    )->setSizePolicy(Widget_SizePolicy::MAXIMUM),
                    $W->Link(
                        $W->Icon('', Func_Icons::MIMETYPES_IMAGE_X_GENERIC),
                        '?tg=addon/portlets/admin&idx=portlet_icon&id='.$definition->getId().'&backend='.$backend->getPath()
                    )->setTitle(portlets_translate('Update the icon of this portlet.')),
                    $W->Link(
                        $W->Icon('', Func_Icons::OBJECTS_TAG),
                        '?tg=addon/portlets/admin&idx=portlet_category&id='.$definition->getId()
                    )->setTitle(portlets_translate('Update category of this portlet.')),
                    $W->CheckBox()
                        ->setTitle(portlets_translate('Uncheck to disable this portlet.'))
                        ->setValue('1')
                        ->setName($definition->getId())
                )->addClass(Func_Icons::ICON_LEFT_32)->setVerticalAlign('middle')
                ->setHorizontalSpacing(1, 'em')
            )->addClass('portlets-admin-definition-frame');

            $backendSection->addItem(
                $definitionSection
            );

            $definitionSection->addClass($cssClasses[$oddEven]);

            $oddEven = 1 - $oddEven;

            $definitionConfigurationActions = $definition->getConfigurationActions();

            if (count($definitionConfigurationActions) > 0) {
                 $definitionSectionMenu = $W->Menu();
                 $definitionSectionMenu->setLayout($W->VBoxLayout())
                     ->addClass(Func_Icons::ICON_LEFT_16);
                 $definitionSection->addItem($definitionSectionMenu, 2);

                 foreach ($definitionConfigurationActions as $definitionConfigurationAction) {
                     /* @var $definitionConfigurationAction Widget_Action */
                     $definitionConfigurationAction->setParameter('backurl', $backurl);
                     $definitionSectionMenu->addAction($definitionConfigurationAction);
                 }
            }

        }

        $form->addItem($backendSection);
    }

    $portletDefinitionConfigurationSet = new portlets_PortletDefinitionConfigurationSet();

    $portletDefinitionConfigurations = $portletDefinitionConfigurationSet->select();
    foreach ($portletDefinitionConfigurations as $portletDefinitionConfiguration) {
        $backend = $portletDefinitionConfiguration->backend;
        $form->setvalue(
            array('definitions', $backend, $portletDefinitionConfiguration->portletDefinition),
            $portletDefinitionConfiguration->active
        );
    }

    $form->addItem($W->SubmitButton()->setLabel(portlets_translate('Save')));
    $page->addItem($form);

    $page->displayHtml();
}



/**
 * Displays acl form to define portlet managers access rights.
 */
function portlet_displayPortletManagers()
{
    global $babBody;
    require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

    $addon = bab_getAddonInfosInstance('portlets');

    $babBody->addItemMenu('backends', portlets_translate('Portlets'), $addon->getUrl().'admin&idx=backends');
    $babBody->addItemMenu('portlet_managers', portlets_translate('Portlet managers'), $addon->getUrl().'admin&idx=portlet_managers');
    $babBody->setCurrentItemMenu('portlet_managers');

    maclGroups();

    $macl = new macl($GLOBALS['babAddonTarget'].'/admin', 'portlet_managers', '0', 'acl');
    $macl->addtable('portlets_manager_groups', portlets_translate('Who can manage portlets?'));
    $macl->babecho();

    $babBody->setTitle(portlets_translate('Portlet managers'));
}



function portlet_setActivePortletDefinitions($definitions)
{
    $portletDefinitionConfigurationSet = new portlets_PortletDefinitionConfigurationSet();

    foreach ($definitions as $backend => $backendDefinitions) {
        foreach ($backendDefinitions as $portletDefinition => $active) {
            $portletDefinitionConfiguration = $portletDefinitionConfigurationSet->get(
                $portletDefinitionConfigurationSet->backend->is($backend)
                ->_AND_($portletDefinitionConfigurationSet->portletDefinition->is($portletDefinition))
            );
            if (!$portletDefinitionConfiguration) {
                $portletDefinitionConfiguration = $portletDefinitionConfigurationSet->newRecord();
                $portletDefinitionConfiguration->backend = $backend;
                $portletDefinitionConfiguration->portletDefinition = $portletDefinition;
            }
            $portletDefinitionConfiguration->active = $active;
            $portletDefinitionConfiguration->save();
        }
    }
}




function portlet_sandbox()
{
    $W = bab_Widgets();

    $page = $W->BabPage();
    $page->setLayout($W->VBoxLayout());

    $page->addItem(
        $W->Title(portlets_translate('Portlets sandbox'), 1)
            ->addClass('title')
    );

    require_once dirname(__FILE__) . '/ovml.php';

    $ovmlPortletContainer = new Func_Ovml_Function_PortletContainer();
    $ovmlPortletContainer->setArgs(
        array(
            'id' => 'portlet_sandbox'
        )
    );


    $page->addItem(
        $W->Html($ovmlPortletContainer->toString())
    );


    $page->displayHtml();
}

function portlet_editPortletIcon()
{
    $addon = bab_getAddonInfosInstance('portlets');

    $W = bab_Widgets();

    $page = $W->BabPage();
    $page->setLayout($W->VBoxLayout());

    $page->addItemMenu('backends', portlets_translate('Portlets'), $addon->getUrl().'admin&idx=backends');
    $page->addItemMenu('portlet_managers', portlets_translate('Portlet managers'), $addon->getUrl().'admin&idx=portlet_managers');
    $page->addItemMenu('icon', portlets_translate('Portlet icon'), $addon->getUrl().'admin&idx=icon');
    $page->setCurrentItemMenu('icon');

    $page->addItem(
            $W->Title(portlets_translate('Portlet icon'), 1)
            ->addClass('title')
    );

    if(bab_gp('save') == 1){
        $GLOBALS['babBody']->addError(portlets_translate('Icon saved'));
    }

    $Form = $W->Form()->setLayout($W->VBoxLayout()->setHorizontalAlign('center'))
    ->setHiddenValue('tg', 'addon/portlets/admin')
    ->setHiddenValue('idx', 'save_portlet_icon')
    ->setHiddenValue('id', bab_gp('id'));

    $AI = bab_getAddonInfosInstance('portlets');
    $upload = $AI->getUploadPath();
    $path = new bab_Path($upload, bab_gp('id'));
    $path->createDir();
    $fileFound = false;
    foreach($path as $file)
    {
        if($path->isFile()){
            $fileFound = true;
            break;
        }
    }

    $Form->addItem(
        $W->VBoxItems(
            $W->FilePicker()
                ->oneFileMode()
                ->setEncodingMethod(Widget_FilePicker::BASE64)
                ->setTitle(portlets_translate('Chose the icon'))
                ->setMaxSize(500000)
                ->setFolder($path)
                ->setAcceptedMimeTypes(array('image/gif', 'image/jpeg', 'image/png', 'image/tiff'))
                ->setName('icon'),
            $W->label(portlets_translate('Replacement can not be undone.')),
            $W->Link(portlets_translate('Return on the list >'), $addon->getUrl().'admin&idx=backends')
        )->setVerticalSpacing(.25, 'em')
    );


    if(!$fileFound){
        /* @var $func func_portletBackend */
        $func = bab_functionality::get(bab_gp('backend'));
        if($func){
            $portDef = $func->getPortletDefinition(bab_gp('id'));
            if($portDef){
                $iconUrl = $portDef->getIcon();
                if ($iconUrl) {
                    $path->push(Widget_FilePicker::BASE64.base64_encode('icon').'.png');
                    copy($iconUrl, $path->tostring());
                    $path->pop();
                }
            }
        }
    }

    /*$Form->addItem($W->SubmitButton()->setLabel(portlets_translate('Save')));*/

    $page->addItem($Form);

    $page->displayHtml();
}

function portlet_editPortletCategory()
{
    require_once dirname(__FILE__).'/category.class.php';
    require_once dirname(__FILE__).'/portletcategory.class.php';

    $I = bab_Functionality::get('Icons');
    $I->includeCss();

    $categorySet = new portlets_CategorySet();
    $portletCategorySet = new portlets_PortletCategorySet();

    $addon = bab_getAddonInfosInstance('portlets');

    $W = bab_Widgets();

    $page = $W->BabPage();
    $page->setLayout($W->VBoxLayout());

    $page->addItemMenu('backends', portlets_translate('Portlets'), $addon->getUrl().'admin&idx=backends');
    $page->addItemMenu('portlet_managers', portlets_translate('Portlet managers'), $addon->getUrl().'admin&idx=portlet_managers');
    $page->addItemMenu('categories', portlets_translate('Portlet categories'), $addon->getUrl().'admin&idx=category');
    $page->setCurrentItemMenu('categories');

    $page->addItem(
            $W->Title(portlets_translate('Portlet categories'), 1)
            ->addClass('title')
    );

    if(bab_gp('save') == 1){
        $GLOBALS['babBody']->addError(portlets_translate('Categories saved'));
    }

    $Form = $W->Form()->setLayout($W->VBoxLayout()->setHorizontalAlign('center'))
        ->setHiddenValue('tg', 'addon/portlets/admin')
        ->setHiddenValue('idx', 'save_portlet_category')
        ->setHiddenValue('id', bab_gp('id'));

    $checkBoxLayout = $W->FlowLayout()->setHorizontalSpacing(1,'em')->setVerticalSpacing(.5, 'em');
    $Form->addItem($checkBoxLayout);

    $potletCategories = $portletCategorySet->select($portletCategorySet->portlet->is(bab_gp('id')));
    $arrayCategory = array();
    foreach($potletCategories as $potletCategory){
        $arrayCategory[$potletCategory->category] = $potletCategory->category;
    }

    $categories = $categorySet->select($categorySet->disabled->is(false));
    foreach($categories as $category){
        $val = isset($arrayCategory[$category->id]);
        $checkBoxLayout->addItem(
            $W->HBoxItems(
                $tmpcheck = $W->CheckBox()->setValue($val)->setName("category[$category->id]"),
                $W->Label($category->name)->setAssociatedWidget($tmpcheck)
            )->setHorizontalSpacing(.25, 'em')
        );
    }

    $Form->addItem($W->SubmitButton()->setLabel(portlets_translate('Save')));

    $page->addItem($Form);

    $page->displayHtml();
}

function portlet_savePortletCategory()
{
    require_once dirname(__FILE__).'/category.class.php';
    require_once dirname(__FILE__).'/portletcategory.class.php';

    $portletCategorySet = new portlets_PortletCategorySet();

    $id = bab_pp('id');
    $categories = bab_pp('category');

    if($id){
        $portletCategorySet->delete($portletCategorySet->portlet->is($id));

        foreach($categories as $category => $v){
            if($v){
                $portletCategory = $portletCategorySet->newRecord();
                $portletCategory->portlet = $id;
                $portletCategory->category = $category;
                $portletCategory->save();
            }
        }

        header('location: ?tg=addon/portlets/admin&idx=portlet_category&save=1&id='.$id);
    }

    $GLOBALS['babBody']->addError(portlets_translate('Categories saved error'));
}

function portlet_editCategory()
{
    require_once dirname(__FILE__).'/category.class.php';
    require_once dirname(__FILE__).'/portletcategory.class.php';

    $categorySet = new portlets_CategorySet();

    $addon = bab_getAddonInfosInstance('portlets');

    $W = bab_Widgets();

    $page = $W->BabPage();
    $page->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));

    $page->addItemMenu('backends', portlets_translate('Portlets'), $addon->getUrl().'admin&idx=backends');
    $page->addItemMenu('portlet_managers', portlets_translate('Portlet managers'), $addon->getUrl().'admin&idx=portlet_managers');
    $page->addItemMenu('categories', portlets_translate('Categories management'), $addon->getUrl().'admin&idx=category');
    $page->setCurrentItemMenu('categories');

    if(bab_gp('save') == 1){
        $GLOBALS['babBody']->addError(portlets_translate('Category saved'));
    }elseif(bab_gp('save') == 2){
        $GLOBALS['babBody']->addError(portlets_translate('Category remove'));
    }

    $Form = $W->Form()->setLayout($W->HBoxItems()->setVerticalAlign('bottom')->setHorizontalSpacing(1,'em'))
        ->setHiddenValue('tg', 'addon/portlets/admin')
        ->setHiddenValue('idx', 'save_category');


    $addForm  = $W->Section(portlets_translate('Add a category'), $Form, 3, 'portlets-add-category')->setFoldable(true, false);

    $Form->addItem(
        $W->VBoxItems(
            $tempLbl = $W->Label(portlets_translate('Category'))->colon(),
            $W->LineEdit()->setMandatory(true, portlets_translate('The name is mandatory.'))->setName('category')->setAssociatedLabel($tempLbl)
        )
    );

    $Form->addItem($W->SubmitButton()->validate()->setLabel(portlets_translate('Add')));

    $page->addItem($addForm);

    $categories = $categorySet->select($categorySet->disabled->is(false));

    $availableColumns = array(
            widget_TableModelViewColumn($categorySet->name, portlets_translate('Category')),
            widget_TableModelViewColumn('_action_', '')
    );

    $tableView = new portlet_categoryTableView();

    $tableView->setDataSource($categories);
    $tableView->setAvailableColumns($availableColumns);
    $tableView->addColumnClass(1, 'thin-column');

    $page->addItem($tableView);

    $page->displayHtml();
}

function portlet_deleteCategory()
{
    require_once dirname(__FILE__).'/category.class.php';
    require_once dirname(__FILE__).'/portletcategory.class.php';

    $categorySet = new portlets_CategorySet();

    $id = bab_gp('id');

    if($id){
        $category = $categorySet->get($categorySet->id->is($id));
        $category->disabled = true;
        $category->save();

        header('location: ?tg=addon/portlets/admin&idx=category&save=2');
    }

    $GLOBALS['babBody']->addError(portlets_translate('Category saved error'));
}

function portlet_saveCategory()
{
    require_once dirname(__FILE__).'/category.class.php';
    require_once dirname(__FILE__).'/portletcategory.class.php';

    $categorySet = new portlets_CategorySet();

    $name = bab_pp('category');

    if($name){
        if(!$category = $categorySet->get($categorySet->name->is($name))){
            $category = $categorySet->newRecord();
            $category->name = $name;
            $category->from = 'user';
            $category->disabled = false;
            $category->save();
        }else{
            $category->disabled = false;
            $category->save();
        }
        header('location: ?tg=addon/portlets/admin&idx=category&save=1');
    }

    $GLOBALS['babBody']->addError(portlets_translate('Category saved error'));
}


if (!bab_isUserAdministrator()) {
    return;
}
switch(bab_rp('idx', 'backends'))
{
    case 'backends':
        portlet_displayAvailableBackends();
        break;

    case 'category':
        portlet_editCategory();
        break;

    case 'delete_category':
        portlet_deleteCategory();
        break;

    case 'save_category':
        portlet_saveCategory();
        break;

    case 'portlet_managers':
        portlet_displayPortletManagers();
        break;

    case 'portlet_category':
        portlet_editPortletCategory();
        break;

    case 'save_portlet_category':
        portlet_savePortletCategory();
        break;

    case 'portlet_icon':
        portlet_editPortletIcon();
        break;

    case 'save_portlet_icon':
        portlet_savePortletIcon();
        break;

    case 'sandbox':
        portlet_sandbox();
        break;

    case 'set_active_portlet_definitions':
        $definitions = bab_rp('definitions', array());
        portlet_setActivePortletDefinitions($definitions);
        portlet_displayAvailableBackends();
        break;
}
