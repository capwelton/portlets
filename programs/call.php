<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
require_once dirname(__FILE__).'/functions.php';






function portlets_loadDockable()
{
	$W = bab_Widgets();

	$canvas = $W->HtmlCanvas();
	$dockable = $W->Dockable(null, null, 'portlet-pickframe');
	$dockable->setTitle(portlets_translate('Select your portlet'));
	$dockable->setOpenFloat();
	$dockable->displayDockButton(false);
	$dockable->displayMainButton(false);
	$dockable->displayDockFullSize(true);

	if (isset($_GET['addon'])) {
		// new controller
		$url = bab_url::get_request('addon');
	} else {
		$url = bab_url::get_request('tg');
	}

	$url->idx = 'dockable';

	$dockable->setUrl($url->toString());

	$html = $dockable->display($canvas);

	return '{
	"dockable"	: '.Widget_HtmlCanvas::json_encode($html).'
	}';
}


function portlets_searchDockable()
{
	$W = bab_Widgets();

	$page = $W->Frame();
	$page->setLayout($W->VBoxLayout());

	$categories = portlets_getCategories();

	$keyword = $W->LineEdit()->setSize(38)->setName('keyword');
	$category = $W->Multiselect()
		->setHeadersDisplay()
		->setFixed()
		->setName('category')
		->setOptions($categories)
		->setSelectedText(portlets_translate('# categories selected'))
		->setSelectText(portlets_translate('Select your categories'));

	$form = $W->Form(null, $W->VBoxLayout());
	$form->addItem(
		$W->FlowItems(
			$W->HBoxItems($W->Label(portlets_translate('By category'))->setAssociatedWidget($category), $category)->setVerticalAlign("middle")->setHorizontalSpacing(1,'em'),
			$W->HBoxItems($W->Label(portlets_translate('By keyword'))->setAssociatedWidget($keyword), $keyword)->setVerticalAlign("middle")->setHorizontalSpacing(1.5,'em')
		)->addClass('portlet-search-form')->setHorizontalSpacing(1,'em')->setVerticalSpacing(1,'em')
	);
	//$form->addItem($W->SubmitButton()->setLabel(portlets_translate('Search'))->addClass('portlet-search-dockable'));
	$page->addItem($form);
	$form->addItem($W->Frame()->addClass('portlet-search-results'));

	return $page->display($W->HtmlCanvas());
}









/**
 * Adds a portlet instance to the specified container.
 *
 * @param string $backend
 * @param string $portlet_definition_id
 * @param string $container               The container identifier
 */
function portlets_createInstance($backend, $portlet_definition_id, $container)
{
	if (!bab_isUserLogged()) {
		return portlets_translate('Access denied');
	}

	$containerConfiguration = portlets_getContainerConfiguration($container);
	if ($containerConfiguration->locked) {
		if (!portlets_userIsPortletManager(null, $container)) {
			return portlets_translate('Access denied');
		}
		$userId = 0;
	} else {
		$userId = bab_getUserId();
	}


	require_once dirname(__FILE__).'/portletconfiguration.class.php';
	require_once dirname(__FILE__).'/frame.class.php';

	/* @var $B Func_PortletBackend */
	$B = bab_Functionality::get('PortletBackend/'.$backend);

	$definition = $B->getPortletDefinition($portlet_definition_id);

	if (null === $definition) {
	    return portlets_translate('Portlet definition not found' . ' (' . $backend . ') ' . $portlet_definition_id);
	}

	// create portlet instance ID

	$portletConfigurationSet = new Portlets_portletConfigurationSet();

	// We want to insert the new portlet at top position.
	$portletConfigurations = $portletConfigurationSet->select($portletConfigurationSet->container->is($container));

	foreach ($portletConfigurations as $portletConfiguration) {
		// We push down all portlets in this container.
		$portletConfiguration->position++;
		$portletConfiguration->save();
	}

	$portletConfiguration = $portletConfigurationSet->newRecord();

	$portletConfiguration->createdOn = date('Y-m-d H:i:s');
	$portletConfiguration->user = $userId;
	$portletConfiguration->container = $container;
	$portletConfiguration->backend = $backend;
	$portletConfiguration->portletDefinition = $portlet_definition_id;
	$portletConfiguration->preferences = serialize(portlet_getDefaultPreferences($portletConfiguration));
	$portletConfiguration->position = 0;

	$portletConfiguration->save();

	$W = bab_Widgets();

	$portletFrame = new portlets_Frame($portletConfiguration->id, '_');
	$portletFrame->setPortlet($portletConfiguration->getPortlet());
	$portletFrame->setRemovable($containerConfiguration->isConfigurable());
	$portletFrame->setConfigurable($containerConfiguration->isConfigurable());

	return $portletFrame->display($W->HtmlCanvas());
}




/**
 * Get preference form for portlet
 * @param int $portlet
 */
function portlets_getInlinePreferencesForm($portlet)
{
	require_once dirname(__FILE__).'/portletconfiguration.class.php';
	$set = new portlets_PortletConfigurationSet();
	$configuration = $set->get($portlet);

	if (null === $configuration) {
		return bab_toHtml(portlets_translate('Configuration not found'));
	}


	$W = bab_Widgets();

	$frame = $configuration->getPreferencesForm();

	if (null === $frame) {
		return bab_toHtml(portlets_translate('Nothing to configure'));
	}

	$frame->additem($W->SubmitButton()->addAttribute('type', 'submit')->addClass('portlet-submit-button')->setLabel(portlets_translate('Save')));

	return $frame->display($W->HtmlCanvas());
}



function portlets_close($portlet)
{
	if (!bab_isUserLogged()) {
		return portlets_translate('Access denied');
	}

	require_once dirname(__FILE__).'/portletconfiguration.class.php';
	$set = new portlets_PortletConfigurationSet();
	$configuration = $set->get($portlet);

	if (null === $configuration) {
		return portlets_translate('Portlet not found');
	}


	if (!$configuration->canRemoveFromContainer()) {
		return portlets_translate('You are not allowed to remove this portlet from this container.');
	}

	$set->delete($set->id->is($portlet));

	return '';
}



/**
 * Modify one preference field
 * return error message or empty string on success
 *
 * @param int $portlet
 * @param string $name
 * @param string $value
 *
 * @return string
 */
function portlets_setPreference($portlet, $name, $value)
{
	if (!bab_isUserLogged()) {
		return portlets_translate('Access denied');
	}

	require_once dirname(__FILE__).'/portletconfiguration.class.php';
	$set = new portlets_PortletConfigurationSet();
	$configuration = $set->get($portlet);

	if (null === $configuration) {
		return portlets_translate('Portlet not found');
	}

	if (!$configuration->canUpdatePreferences()) {
		return portlets_translate('You are not allowed to update preferences on this portlet.');
	}

	$configuration->setPreference($name, $value);

	$configuration->save();

	return '';
}



function portlets_savePreferences($portlet, $preferences, $container)
{
	if (!bab_isUserLogged()) {
		return portlets_translate('Access denied');
	}

	require_once dirname(__FILE__).'/portletconfiguration.class.php';
	require_once dirname(__FILE__).'/frame.class.php';
	$set = new portlets_PortletConfigurationSet();
	$configuration = $set->get($portlet);

	if (null === $configuration) {
		return portlets_translate('Portlet not found');
	}

	if ($preferences['_blockTitleType'] === 'portlet') {
	    $preferences['_blockTitle'] = '_';
	}

	if (!$configuration->canUpdatePreferences()) {
		return portlets_translate('You are not allowed to update preferences on this portlet.');
	}

	foreach ($preferences as &$value) {
		if (is_array($value)) {
			foreach ($value as &$value2) {
				$value2 = bab_convertToDatabaseEncoding($value2);
			}
		} else {
			$value = bab_convertToDatabaseEncoding($value);
		}
	}
	$configuration->preferences = serialize($preferences);
	$configuration->save();

	$W = bab_Widgets();

	$containerConfiguration = portlets_getContainerConfiguration($container);

	$title = null;
	if (isset($preferences['_blockTitle']) && !empty($preferences['_blockTitle'])) {

		$title = $preferences['_blockTitle'];
	}

	$portletFrame = new portlets_Frame($configuration->id, $title);
	$portletFrame->setPortlet($configuration->getPortlet());
	$portletFrame->setRemovable($containerConfiguration->isConfigurable());
	$portletFrame->setConfigurable($containerConfiguration->isConfigurable());

	if (isset($preferences['_blockWidth']) && !empty($preferences['_blockWidth'])) {
	    $portletFrame->setSizePolicy($preferences['_blockWidth']);
	}
	if (isset($preferences['_blockHeight']) && !empty($preferences['_blockHeight'])) {
	    $result = portlet_preferenceHeightToArray($preferences['_blockHeight']);
	    
	    $height = $result['value'];
	    $unit = $result['unit'];
	    
	    $portletFrame->setCanvasOptions(Widget_Item::options()->height($height, $unit));
	}
	if (isset($preferences['_blockClassname']) && !empty($preferences['_blockClassname'])) {
	    $portletFrame->addClass($preferences['_blockClassname']);
	}
	return $portletFrame->display($W->HtmlCanvas());
}



// function portlets_get($portlet)
// {
// 	if (!bab_isUserLogged()) {
// 		return portlets_translate('Access denied');
// 	}

// 	require_once dirname(__FILE__).'/portletconfiguration.class.php';
// 	$set = new portlets_PortletConfigurationSet();
// 	$configuration = $set->get($portlet);

// 	if (null === $configuration) {
// 		return portlets_translate('Portlet not found');
// 	}


// 	if (((int)bab_getUserId()) !== (int) $configuration->user) {
// 		return portlets_translate('Access denied');
// 	}

// 	$configuration->preferences = serialize($preferences);
// 	$configuration->save();

// 	return '';
// }




/**
 * Saves the order of portlets in the specified container.
 *
 * @param string $container The container id
 * @param array  $portlets  An array of portlet id indexed by portlet positions.
 *
 * @return string
 */
function portlets_movePortlets($container, $portlets)
{
	require_once dirname(__FILE__).'/portletconfiguration.class.php';
	$set = new portlets_PortletConfigurationSet();

	$containerConfiguration = portlets_getContainerConfiguration($container);
	if (!$containerConfiguration->isSortable()) {
		return portlets_translate('You are not allowed to reorder portlets in this container.');
	}

	if (is_array($portlets)) {
		foreach ($portlets as $position => $portlet) {
			$record = $set->get($portlet);

			if (((int) $position) !== (int) $record->position || $container != $record->container) {
				$record->container = $container;
				$record->position = $position;
				$record->save();
			}
		}
	}

	return '';
}



/**
 * @param string $container The container identifier.
 *
 * @return string
 */
function portlets_toggleLockContainer($container)
{
	require_once dirname(__FILE__).'/containerconfiguration.class.php';

	if (!portlets_userIsPortletManager(null, $container)) {
		return portlets_translate('You are not allowed to lock/unlock this porltet container.');
	}

	$containerConfigurationSet = new portlets_ContainerConfigurationSet();

	$containerConfigurations = $containerConfigurationSet->select($containerConfigurationSet->container->is($container));

	$containerConfiguration = null;
	foreach ($containerConfigurations as $containerConfiguration) {
		break;
	}
	if (!isset($containerConfiguration)) {
		$containerConfiguration = $containerConfigurationSet->newRecord();
		$containerConfiguration->container = $container;
		$locked = true;
	} else {
		$locked = !$containerConfiguration->locked;
	}
	$containerConfiguration->locked = $locked;

	$containerConfiguration->save();

	return portlets_getContainerHtml($container);
}



/**
 * Get categories to search in
 * merge from all backends
 * @return array
 */
function portlets_getCategories()
{
	portlets_populateCategory();
	require_once dirname(__FILE__).'/category.class.php';
	require_once dirname(__FILE__).'/portletcategory.class.php';

	$categoryPortletSet = new portlets_PortletCategorySet();
	$categoryPortlets = $categoryPortletSet->select();
	$categoryPortletArray = array();
	foreach($categoryPortlets as $categoryPortlet){
		$categoryPortletArray[] = $categoryPortlet->category;
	}

	$categorySet = new portlets_CategorySet();

	$categories = $categorySet->select($categorySet->id->in($categoryPortletArray)->_AND_($categorySet->disabled->is(false)));

	$categoriesArray = array();
	foreach($categories as $category){
		$categoriesArray[$category->id] = $category->name;
	}

	bab_Sort::natcasesort($categoriesArray);
	return $categoriesArray;
}



/**
 * get html list of portlets do display in the dock for selection
 * @param string $keyword
 * @param string $category
 *
 * @return string
 */
function portlets_searchPortlet($keyword = null, $category = null)
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	require_once dirname(__FILE__) . '/portletdefinitionconfiguration.class.php';
	require_once dirname(__FILE__) . '/category.class.php';
	require_once dirname(__FILE__) . '/portletcategory.class.php';

	$portletDefinitionConfigurationSet = new portlets_PortletDefinitionConfigurationSet();

	if ('' === $category) {
		$category = null;
	} else {
		$category = urldecode(utf8_decode($category));
		$category = explode(',', $category);
	}


	if ('' === $keyword) {
		$keyword = null;
	}


	$W = bab_Widgets();
	$func = new bab_Functionalities();

	$backendNames = $func->getChildren('PortletBackend');

	bab_functionality::includefile('PortletBackend');
	bab_functionality::includefile('Icons');
	$result = $W->FlowLayout();
	$result->setVerticalSpacing(10)
		->setHorizontalSpacing(10)
		->setVerticalAlign('top')
		->addClass('portlet-result-table')
		->addClass(Func_Icons::ICON_LEFT_48);


	$portletsInCategory = array();
	if ($category) {
		$categoryPortletSet = new portlets_PortletCategorySet();
		$categoryPortletSet->category();

		$categoryPortlets = $categoryPortletSet->select($categoryPortletSet->category->id->in($category));
		foreach ($categoryPortlets as $categoryPortlet) {
			$portletsInCategory[$categoryPortlet->portlet] = $categoryPortlet->portlet;
		}
	}

	$col = 0;
	$row = 0;
	foreach ($backendNames as $backendName) {
		$backendPath = trim('PortletBackend/' . $backendName, '/');

		/* @var $backend Func_PortletBackend */
		$backend = bab_Functionality::get($backendPath);

		if (!$backend) {
		    bab_debug('Portlet backend unavailable: ' . $backendPath);
		    continue;
		}

		$portletDefinitions = $backend->select();
		$nbPortletDefinitions = 0;

		foreach ($portletDefinitions as $portletDefinition) {

		    $portletDefinitionId = $portletDefinition->getId();

		    if ($category && !isset($portletsInCategory[$portletDefinitionId])) {
		        continue;
		    }

		    if (!empty($keyword)) {
				$search1 = mb_strtolower($keyword);
				$search2 = mb_strtolower($portletDefinition->getName()) . ' ' . mb_strtolower($portletDefinition->getDescription());

				if (false === mb_strpos($search2, $search1)) {
					continue;
				}
			}

			$portletDefinitionConfiguration = $portletDefinitionConfigurationSet->get(
			    $portletDefinitionConfigurationSet->backend->is($backendName)
			    ->_AND_($portletDefinitionConfigurationSet->portletDefinition->is($portletDefinitionId))
		    );

			if (isset($portletDefinitionConfiguration) && !$portletDefinitionConfiguration->active) {
			    continue;
			}

			$nbPortletDefinitions++;

			$iconUrl = $portletDefinition->getIcon();
			$icon = $W->Image($iconUrl)->setCanvasOptions(Widget_Item::Options()->width(48, 'px'));

			$frame = $W->Frame(null, $W->HboxLayout()->setVerticalAlign('middle')->setSpacing(0, 'px', 5, 'px'));
			$frame->addClass('portlet-definition')
				->addItem($icon)
				->addItem(
					$W->VBoxItems(
						$W->Label($portletDefinition->getName())->addClass('portlet-title'),
						$W->Label($portletDefinition->getDescription())->addClass('portlet-description')
					)
				)->setTitle($portletDefinition->getDescription())
				->setMetadata('backend', $backendName)
				->setMetadata('portlet_definition_id', $portletDefinitionId);

			$frame->setSizePolicy('portlet-search-layout-item');

			$result->addItem($frame/*, $row, $col*/);

			$col++;
			if ($col == 3) {
				$col = 0;
				$row++;
			}
		}
	}

	return $result->display($W->HtmlCanvas());
}



switch (bab_rp('idx', null))
{
	case 'loaddockable':
		die(portlets_loadDockable());

	case 'dockable':
		die(portlets_searchDockable());

	case 'create': // create portlet instance an return portlet html
		die(portlets_createInstance(bab_rp('backend'), bab_rp('portlet_definition_id'), bab_rp('container')));

// 	case 'get':
// 		die(portlets_get(bab_rp('portlet')));

	case 'preferences':
		die(portlets_getInlinePreferencesForm(bab_rp('portlet')));

	case 'close':
		die(portlets_close(bab_rp('portlet')));

	case 'setPreference':
		die(portlets_setPreference(bab_rp('portlet'), bab_rp('name'), bab_rp('value')));

	case 'savePreferences':
		die(portlets_savePreferences(bab_rp('portlet'), bab_rp('preferences'), bab_rp('container')));

	case 'movePortlets':
		die(portlets_movePortlets(bab_rp('container'), bab_rp('portlets')));

	case 'toggleLockContainer';
		die(portlets_toggleLockContainer(bab_rp('container')));

	case 'searchPortlet':
		die(portlets_searchPortlet(bab_rp('keyword', null), bab_rp('category', null)));
}
