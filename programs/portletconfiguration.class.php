<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


// Initialize mysql ORM backend.
bab_functionality::get('LibOrm')->initMySql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($GLOBALS['babDB']));



/**
 *
 * @property ORM_PkField       $id
 * @property ORM_DateTimeField $createdOn
 * @property ORM_IntField      $user
 * @property ORM_StringField   $container          The container id.
 * @property ORM_StringField   $backend
 * @property ORM_StringField   $portletDefinition
 * @property ORM_TextField     $preferences
 * @property ORM_TextField     $position
 *
 * @method portlets_PortletConfiguration    newRecord()
 * @method portlets_PortletConfiguration    get()
 * @method portlets_PortletConfiguration[]  select()
 */
class portlets_PortletConfigurationSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_DateTimeField('createdOn')
                ->setDescription('Created on'),
            ORM_IntField('user'),
            ORM_StringField('container'),
            ORM_StringField('backend'),
            ORM_StringField('portletDefinition'),
            ORM_TextField('preferences'),
            ORM_StringField('position')
        );
    }
}


/**
 *
 * @property int    $id
 * @property string $createdOn
 * @property int    $user
 * @property string $container
 * @property string $backend
 * @property string $portletDefinition
 * @property string $preferences
 * @property string $position
 */
class portlets_PortletConfiguration extends ORM_MySqlRecord
{
    /**
     * @return Func_PortletBackend
     */
    public function getBackend()
    {
        return bab_functionality::get('PortletBackend/'.$this->backend);
    }

    /**
     * @return portlet_PortletDefinitionInterface
     */
    public function getDefinition()
    {
        return $this->getBackend()->getPortletDefinition($this->portletDefinition);
    }


    /**
     * get array of fields to use to save the preferences
     * @return array
     */
    public function getPreferences()
    {
        return $this->getDefinition()->getPreferenceFields();
    }



    /**
     * Returns the array of current preferences values. If a preference is not set, the default
     * value is used.
     *
     * @return array
     */
    public function getPreferencesValues()
    {
        $preferences = array();
        $portletDefinition = $this->getDefinition();
        $settings = $portletDefinition->getPreferenceFields();
        foreach ($settings as $setting) {
            if (isset($setting['default'])) {
                $preferences[$setting['name']] = $setting['default'];
            }
        }
        if ('' !== $this->preferences) {
            $currentPreferences = unserialize($this->preferences);
            foreach ($currentPreferences as $k => $v) {
                $preferences[$k] = $v;
            }
        }

        return $preferences;
    }


    /**
     * @return Widget_Form
     */
    public function getPreferencesForm()
    {
        require_once dirname(__FILE__).'/preferencesform.php';

        $portletDefinition = $this->getDefinition();
        $form = portlets_getPreferencesForm($portletDefinition);

        $values = $this->getPreferencesValues();

        if (isset($values['_blockTitleType']) && $values['_blockTitleType'] === 'portlet') {
            $values['_blockTitle'] = '';
        }

        if (!empty($values)) {
            $form->setValues(array('preferences' => $values));
        }

        return $form;
    }



    /**
     * Get portlet displayable item
     * Without frame, used
     * @return portlet_PortletInterface
     */
    public function getPortlet()
    {
        $portlet = $this->getBackend()->getPortlet($this->portletDefinition);

        $portlet->setPortletId($this->id);

        $values = $this->getPreferencesValues();

        $portlet->setPreferences($values);
        return $portlet;
    }




    /**
     * Modify one preference field.
     *
     * @since 0.22.0
     *
     * @param string $name
     * @param string $value
     *
     * @return self
     */
    public function setPreference($name, $value)
    {
        $preferences = $this->getPreferencesValues();

        $preferences[$name] = $value;

        $this->preferences = serialize($preferences);

        return $this;
    }


    /**
     * Check if he current user can update the portlet preferences.
     *
     * @param int $userId
     * @return boolean
     */
    public function canUpdatePreferences($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }
        return $this->user == $userId || ($this->user == 0 && portlets_userIsPortletManager($userId, $this->container));
    }


    /**
     * Check if he current user can remove the portlet from its container.
     *
     * @param int $userId
     * @return boolean
     */
    public function canRemoveFromContainer($userId = null)
    {
        if (!isset($userId)) {
            $userId = bab_getUserId();
        }
        return $this->user == $userId || ($this->user == 0 && portlets_userIsPortletManager($userId, $this->container));
    }
}
