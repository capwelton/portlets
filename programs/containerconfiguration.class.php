<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


// Initialize mysql ORM backend.
bab_functionality::get('LibOrm')->initMySql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($GLOBALS['babDB']));



/**
 * @property ORM_StringField $container
 * @property ORM_BoolField   $locked
 *
 * @method  portlets_ContainerConfiguration get()
 * @method  portlets_ContainerConfiguration newRecord()
 */
class portlets_ContainerConfigurationSet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('container'),
			ORM_BoolField('locked')
		);

	}
}


/**
 * @property string $container
 * @property bool   $locked
 */
class portlets_ContainerConfiguration extends ORM_MySqlRecord
{
    /**
     * Checks that the container portlets can be reordered by the specified user.
     *
     * @param string $userId    The user id or null for the current user.
     * @return boolean
     */
	public function isSortable($userId = null)
	{
		return bab_isUserLogged($userId ? $userId : '') && (!$this->locked || portlets_userIsPortletManager($userId, $this->container));
	}

	/**
	 * Checks that the container portlets can be configured by the specified user.
	 *
     * @param string $userId    The user id or null for the current user.
  	 * @return boolean
	 */
	public function isConfigurable($userId = null)
	{
		return bab_isUserLogged($userId ? $userId : '') && (!$this->locked || portlets_userIsPortletManager($userId, $this->container));
	}

    /**
     * Checks that the container can be locked by the specified user.
     *
     * @param string $userId    The user id or null for the current user.
     * @return boolean
     */
	public function isLockable($userId = null)
	{
		return bab_isUserLogged($userId ? $userId : '') && portlets_userIsPortletManager($userId, $this->container);
	}

	/**
	 * Gives manager access to this container for the specified group.
	 *
	 * @param int $groupId
	 * @return boolean
	 */

	public function addPortletManagerGroup($groupId)
	{
        return aclAdd('portlets_manager_groups', $groupId, $this->id);
	}


	/**
	 * Removes manager access to this container for the specified group.
	 *
	 * @param int $groupId
	 * @return boolean
	 */
	public function removePortletManagerGroup($groupId)
	{
        return aclRemove('portlets_manager_groups', $groupId, $this->id);
	}
}
